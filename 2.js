// Ejercicio 2
// Dada una matriz de N elementos repetidos,
// crea una función numbersTop para obtener los tres elementos más repetidos ordenados de forma descendente por número de repeticiones.

const numbersTop = (array) => {
    return [...new Set(array)] // Quitamos los duplicados. Para tener nuesto objeto de ir sumando su propiedad 1 +1 siempre y cuando se encuentren parecidos en el recoddigo del .map
        .map(num => ({ key: num, count: array.filter(i => i === num).length })) //acá  accdeos al [key]de  la propiedad del objeto que acabamos de crear. de esta forma posterioemente, en el filter. busca sus parecidos y regresa el length
        .sort((a, b) => b.count - a.count) //ordenamos
        .slice(0, 3) //solo los primers 3
        .map(({ key }) => key)// imprimiros los keys- no los values de los keys del obj
}


/**
 * TEST Ejercicio 2
 */
numbersTop([3, 3, 1, 4, 1, 3, 1, 1, 2, 2, 2, 3, 1, 3, 4, 1]) // [ 1, 3, 2 ]
numbersTop(['a', 3, 2, 'a', 2, 3, 'a', 3, 4, 'a', 'a', 1, 'a', 2, 'a', 3]) // [ 'a', 3, 2 ]
