// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

/**
 * 
 * En la función de flecha findUniq 
 *   Le pasamos el arreglo para buscar exactamente  el array de
 *  Primero de adentro hacía afuera explicando el código
 *  EL filter nos ayuda a buscar todos los coincidentes por ejemplo [Hulks] pero hay más de 1 Hulk que es donde se compara el array de encontrados por el filter
 *  después como la condición no se cumple de que la longitud del array_encontrador no es igual a 1. de igual forma sigue iterando hasta que se cumple al  al menos que tenga su Longitud d = 1
 */

const findUniq = (array) => array.find(item => array.filter(search => search === item).length === 1)


/**
 * TEST Ejercicio 1
 */
findUniq(['12', 10, '12', 11, 1, 11, 10, '12']) // 1
findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man']) // 'Wonder Woman'
